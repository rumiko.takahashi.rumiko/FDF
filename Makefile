#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/25 14:35:28 by vbudnik           #+#    #+#              #
#    Updated: 2018/04/18 20:18:12 by vbudnik          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = fdf
INC_DIR = ./includes/
SRCS_DIR = ./srcs/

SRCS_FILES = fdf.c get_values.c draw_line.c valid.c valid_2.c

SRCS = $(addprefix $(SRCS_DIR), $(SRCS_FILES))

OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME) : $(OBJS)
			@make -C libft/
			@gcc -o $@ $^ -L libft/ -lft -L minilibx_macos/ -lmlx -framework OpenGL -framework AppKit
			@echo "MAKING $(NAME)! : \033[1;32m DONE !\033[m"

clean:
			@rm -f $(OBJS)
			@echo "CLEANING! : \033[1;31m DONE !\033[m"

fclean: clean
			@rm -f $(NAME)
			@cd libft && make fclean
			@echo "FCKEANING! : \033[1;31m DONE !\033[m"

re: fclean all

.PHONY: all clean fclean re
