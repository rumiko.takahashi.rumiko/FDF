/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 21:06:46 by vbudnik           #+#    #+#             */
/*   Updated: 2018/04/19 21:06:50 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	valid(int ac, char **av)
{
	struct stat		sb;

	if (ac == 1)
	{
		ft_putendl("You nead input mane map");
		exit(0);
	}
	if (ac > 2)
	{
		ft_putendl("Only one arguments");
		exit(0);
	}
	stat(av[1], &sb);
	if (!S_ISREG(sb.st_mode))
	{
		ft_putendl("File not map");
		exit(0);
	}
}

void	valid_size(char **tmp, int x)
{
	int		i;

	i = 0;
	while (tmp[i] != NULL)
		i++;
	if (i != x)
	{
		ft_putendl("Check size of map");
		ft_putendl("Only X*X size map");
		ft_putendl("X = X");
		exit(0);
	}
}

void	len_hex(char *tmp)
{
	int		i;
	int		j;
	int		flag;

	i = 0;
	j = 0;
	flag = 0;
	while (tmp[i] != '\0')
	{
		if (tmp[i] == ',')
		{
			flag = 1;
			j = i;
			while (tmp[i] != '\0')
				i++;
		}
		i++;
	}
	i = i - 2;
	if (i - j == 8 || flag == 0)
		;
	else
		ft_exit_mas("Not valid format of hex number");
}

void	valid_item(char *tmp)
{
	int		i;

	i = 0;
	len_hex(tmp);
	if (tmp[0] == '-')
		i++;
	while (tmp[i] != '\0')
	{
		if (ft_isdigit(tmp[i]))
			i++;
		else if (tmp[i] == ',')
		{
			if (tmp[i + 1] == '0' && tmp[i + 2] == 'x')
				while (tmp[++i] != '\0')
					exit_hex(tmp[i]);
			else
				ft_exit_mas("Only hex number for color");
		}
		else
			ft_exit_mas("Cheack you file");
	}
}
