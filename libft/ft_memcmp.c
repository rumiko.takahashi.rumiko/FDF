/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 15:12:09 by vbudnik           #+#    #+#             */
/*   Updated: 2017/11/11 14:29:03 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	const unsigned char	*su1;
	const unsigned char	*su2;
	int					res;

	res = 0;
	su1 = s1;
	su2 = s2;
	while (0 < n)
	{
		if ((res = *su1 - *su2) != 0)
			break ;
		++su1;
		++su2;
		n--;
	}
	return (res);
}
